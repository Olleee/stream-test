var gulp = require('gulp');
 	  stylus = require('gulp-stylus'),
 	  livereload = require('gulp-livereload'),
    // connect = require('gulp-connect'),
    pug = require('gulp-pug'),
    watch = require('gulp-watch');
// pl09_



gulp.task('styles', function() {
   	return gulp.src('./source/styles/main.styl')
    	.pipe( stylus())
    	.pipe( gulp.dest('./public/css/') );


});

gulp.task('pages', function(){
  return gulp.src('./source/pages/*.pug')
      .pipe( pug({pretty:true}))
      .pipe( gulp.dest('./public') );

      
});

gulp.task('watch', function() {
      gulp.watch(['./source/styles/main.styl','./source/**/*.styl',],['styles']);
      gulp.watch('./source/**/*.pug',['pages']);
});

gulp.task('default', ['pages','styles','watch']);


